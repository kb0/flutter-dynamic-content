import 'package:easy_logger/easy_logger.dart';

final EasyLogger logger = EasyLogger(
  name: 'downloadable_content',
);
